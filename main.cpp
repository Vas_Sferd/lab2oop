/*
 * main.cxx
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <iostream>
#include <iterator>
#include <utility>
#include <set>

// Лабораторная работа № 2
// Вариант 20
// Необходимо написать программу на языке C++
// 0 - Выход
// 1 Метод: Объединение двух множеств. 			Пример: C = A + B
// 2 Метод: Разность двух множеств.    			Пример: C = A / B
// 3 Метод: Добавление числа.					Пример: A += b
// 4 Метод: Удаление числа.						Пример: A /= b
// 5 Метод: Объединение множества и числа.		Пример: 
// 6 Метод: Объединение числа и множества.      Пример: ()
// 7 Метод: Разность множества и числа          Пример: ()


class Bunch 
{
    std::set<double> storage;

public:
    Bunch& operator += (double num) { storage.insert(num); return std::move(*this); }
    friend std::ostream& operator << (std::ostream& os, const Bunch& bunch)
    {
        std::copy(bunch.storage.begin(), bunch.storage.end(), std::ostream_iterator<double>(os, " "));
        return os;
    }
};	

class BunchBuilder
{
    Bunch& bunch;

public:
    BunchBuilder()              : bunch(std::move(*new Bunch))  {}
    BunchBuilder(Bunch& bunch)  : bunch(std::move(bunch))       {}
    BunchBuilder& add(double e) { bunch += e; return std::move(*this); }
    Bunch& build()              { return std::move(bunch); }
};

int main(int argc, char **argv)
{
    auto first = BunchBuilder()
        .add(1)
        .add(2)
        .build();

    std::cout << "Before: " << first << std::endl;

    int n; 
    for (std::cin >> n; n > 0; --n)
    {
        double e;
        std::cin >> e;
        first += e;
    }

    std::cout << "After all operation: " << first << std::endl;

    return 0;
}
